# -*- coding: utf-8 -*-
#
# Copyright © 2016 Mathieu Duponchelle <mathieu.duponchelle@opencreed.com>
# Copyright © 2016 Collabora Ltd
#
# This library is free software; you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation; either version 2.1 of the License, or (at your option)
# any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this library.  If not, see <http://www.gnu.org/licenses/>.

import os
import io

from hotdoc.core.base_extension import BaseExtension
from hotdoc.core.base_formatter import Formatter

DESCRIPTION=\
"""
Implements a formatter that outputs files more easily
usable in apertis SDK portal.
"""


class ApertisExtension(BaseExtension):
    EXTENSION_NAME = 'apertis-extension'
    argument_prefix = 'apertis'

    def __init__(self, doc_repo):
        BaseExtension.__init__(self, doc_repo)

    def setup(self):
        Formatter.writing_page_signal.connect(self.__writing_page_cb)
        for ext in self.doc_repo.extensions.values():
            formatter = ext.formatters.get('html')
            here = os.path.dirname(__file__)
            template_path = os.path.join(here, 'templates')
            formatter.engine.loader.searchpath.insert(0, template_path)

    def __writing_page_cb(self, formatter, page, path):
        stripped = os.path.splitext(path)[0]
        scripts = page.output_attrs['html']['scripts']
        stylesheets = page.output_attrs['html']['stylesheets']

        with io.open(stripped + '.scripts', 'w', encoding='utf-8') as _:
            for script in scripts:
                _.write(u'%s\n' % os.path.basename(script))
            _.write(u'typeahead.jquery.js\n')
            _.write(u'utils.js\n')
            _.write(u'navigation.js\n')
            _.write(u'search.js\n')

        with io.open(stripped + '.stylesheets', 'w', encoding='utf-8') as _:
            for stylesheet in stylesheets:
                _.write(u'%s\n' % os.path.basename(stylesheet))
            _.write(u'sidenav.css\n')
            _.write(u'search.css\n')
            _.write(u'tables.css\n')

def get_extension_classes():
    return [ApertisExtension]
