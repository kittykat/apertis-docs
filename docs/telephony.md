---
short-description: "Telephony services"

authors:
    - name: Robert Bosch Car Multimedia GmbH
      years: [2015, 2016]

license: CC-BY-SAv4.0
...

# Telephony

### Provides:

- Call handling

### Enabling components:

- ofono - includes consistent, minimal, and easy to use complete APIs
  for telephony (https://01.org/ofono)
