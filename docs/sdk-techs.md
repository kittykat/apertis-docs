---
short-description: "Presentation of our software stack"

authors:
    - name: Robert Bosch Car Multimedia GmbH
      years: [2015, 2016]

license: CC-BY-SAv4.0
...

# Technologies

Apertis follows an app-centric architecture. This means that the
development is more vertical and not layered as in automotive domain.
This reduces dependency and leads to less time to market.

SDK is designed to make app development fast and easy while maintaining
backward compatibility. The system gets upgraded periodically; however
the applications should not be affected. This is achieved by writing
apps against SDK APIs. The SDK APIs are ensured to maintained backward
compatibility.

The SDK will not only expose native system services integrated into the
OS, but also complete high level features which are of interest for
multiple Applications, Due to it’s nature, we classify the APIs into
Enabling APIs and SDK APIs.

## Enabling APIs

These are APIs from libraries/services that are typically taken from
Open Source (for example, Telepathy, Tracker). Apps which use these APIs
directly need to maintain the stability on their own due to API or ABI
breakages. Effort may be made to ease or remove transition issues when
ABI or API breakages happen.

Trying to encapsulate these into SDK APIs will be a continuous process
within apertis.

## SDK APIs

They realize high level features of the SDK behind the scene (e.g. an
adressbook service which can be used by Apps to store and retrieve
contact data in a joint place). They are dedicated APIs, provided with
good documentation and sample code, easy to use and with backward
compatibility guaranteed exposed with the intention to be used by the
Apps.

> The SDK services are developed as dbus services or libraries. For the APIs please refer the API reference or DevHelp on desktop.
