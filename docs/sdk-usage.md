---
short-description: "Collection of guides for using the SDK"

authors:
    - name: Robert Bosch Car Multimedia GmbH
      years: [2015, 2016]
    - name: Micah Fedke
      email: micah.fedke@collabora.co.uk
      years: [2016]

license: CC-BY-SAv4.0
...

# Using the SDK

## Overview of SDK features
Take a moment to familiarize yourself with some of the more commonly used tools in the SDK.  These include the file browser, Eclipse, DevHelp and the simulator.  All of these tools have shortcut links placed on the desktop.

## File browser
![](media/overview-home.png)
This icon opens the file manager so you can access the local folders and use the shared folder for exchanging documents between your host system and the virtual machine. You should be able to find your shared folder under **Places** on the left hand side of the file manager window.

## Eclipse IDE
![](media/overview-eclipse.png)
Eclipse is the IDE used for application development in the SDK. Eclipse includes a custom Apertis plugin for updating the sysroot from within Eclipse.

## Apertis API documentation
![](media/overview-docs.png)
DevHelp is the standard browser for offline API documentation across many Open Source projects. The DevHelp installation in the SDK provides a local, offline version of the all the documentation found in the Application Developer Portal, including the Apertis API references.

---

Congratulations!  You now have everything you need to start developing applications for Apertis.

### Next

|-|
| ![](media/continue-arrow.png) **Start developing your app in the SDK.** [Continue on to application development](app-dev.md) |
