---
short-description: "Add your application to the Apertis app store"

authors:
    - name: Ekaterina Gerasimova
      email: kat@collabora.co.uk
      years: [2016]

license: CC-BY-SAv4.0
...

# Distribute your application

Once you have written your application, you can submit your application to the Apertis app store. All applications are reviewed before they are distributed to users. Accepted applications will become available for download by users on their Apertis devices through the appstore.

Once your consider your application ready for distribution, you will need to create a tarball. You can submit the tarball of your application to the app store through the application developer portal: log into your account (you should already have one from downloading the SDK), and [submit a new application](https://developer.apertis.org/app/create). If you need to update your application, you can do this from [your list of applications](https://developer.apertis.org/app/).

You will need to fill in some basic information about your application such as the title, category and decription. You can also fill in the optional fields to provide additional information about your application which may speed up the review process and help users find out more about your app. Attach your application as a tarball and submit it.

Once the tarball is uploaded, it will be reviewed by the app store curators for compliance, security and performance. If there any any issues with your app, you will be contacted through the appdev portal to discuss how they can be resolved.

Your app will become available through the app store shortly after it is accepted.

If you want to release a new version of your application or upload an updated tarball, you can do this from the application page, which you can find in your [list of applications](https://developer.apertis.org/app/). Your application will have to go through a new review every time that you submit an update. This will ensure an overall high quality of applications in the app store.
