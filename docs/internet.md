---
short-description: "Various internet utilities provided by the SDK"

authors:
    - name: Robert Bosch Car Multimedia GmbH
      years: [2015, 2016]

license: CC-BY-SAv4.0
...

# Internet services

### Provides:

- http clients

- Connected services (PIM)

### Enabling components:

- libcurl - the multiprotocol file transfer library
  (http://libcurl.org/)

- libsoup - HTTP client/server library for GNOME
  (https://wiki.gnome.org/action/show/Projects/libsoup)

- libecal (Evolution Data Sever)
  (https://developer.gnome.org/libecal/stable/)

- libfolks-eds: contacts from multiple sources with
  Evolution-data-server as backend
  (https://wiki.gnome.org/Projects/Folks)
