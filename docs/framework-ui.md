---
short-description: "The UI framework"

authors:
    - name: Robert Bosch Car Multimedia GmbH
      years: [2015, 2016]

license: CC-BY-SAv4.0
...

# UI framework

### Provides:

- In apertis, Clutter library is provided for UI creation.

- Clutter is an open source software library for creating fast,
  compelling, portable, and dynamic graphical user interfaces.

- Use Open GLES for rendering.

- Comes with rich animation f/w with vast gesture support.

- Provides layout management for easy lay-outing of widgets.

### SDK services:

- Libthornbury: libthornbury is a bunch of user Interface utility
  libraries. Thornbury provides the helper function for customized
  json parsing which is used in view manager, texture creation for
  static .png files or download from the web
  sychronously/asynchronously using libsoup to create the texture
  using GdkPixbuf,View manager is intended to create/manage the views
  in an app using Clutter, ItemFactory is used to instantiate any
  given widget type which implements MxItemfactory. Model to give the
  MVC approach to the app inspired by ClutterModel

- Liblightwood: liblightwood is library for widgets.It can also be
  extended to a different set of requirements. Liblightwood has all
  basic widgets like button,list,roller,webview widgets,multiline
  textbox.etc
