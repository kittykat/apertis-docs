# Documentation for the Apertis developer portal

This is the source for the documentation of the Apertis Developer Portal, hosted at <https://appdev.apertis.org/>. The website sources can be found at <https://git.internal.apertis.org/cgit/users/meh/backworth.git/> , instructions on how to run it can be found in this project's README.

This project contains a specialized theme and a hotdoc extension that should be used when building the documentation for the website, however using these is not required for submitting contributions, the process for doing this is explained hereafter.

> If building the documentation for usage in backworth, skip the next sections and go directly to the `Building the documentation for usage on appdev.apertis.org` section.

## Building the documentation standalone on Apertis

This project [can be built as a Debian-format
package](https://www.debian.org/doc/manuals/maint-guide/build.en.html)
on Apertis systems. When the resulting `apertis-docs` package is installed,
the HTML documentation is installed in
[/usr/share/doc/apertis-docs/html](file:///usr/share/doc/apertis-docs/html/)
and can be viewed with this command:

```
xdg-open file:///usr/share/doc/apertis-docs/html/index.html
```

## Building the documentation standalone on other systems

* Make sure pip and virtualenv are installed on your system, on Ubuntu:

    ```
    sudo apt-get install python-pip
    sudo pip install virtualenv
    ```

* Install hotdoc dependencies, on Ubuntu:

    ```
    sudo apt-get install python-dev libgraphviz-dev libxml2-dev libxslt1-dev cmake
    ```

    and on Fedora:

    ```
    sudo dnf install graphviz-devel python-devel libxml2-devel libxslt-devel cmake
    ```

* Install a recent enough version of hotdoc (>= 0.7.9):

    ```
    virtualenv hotdoc_venv
    . hotdoc_venv/bin/activate
    pip install hotdoc hotdoc-search-extension hotdoc-devhelp-extension
    ```

* Build the documentation:

    ```
    hotdoc run
    ```

* Look at the result:

    ```
    xdg-open built_doc/html/index.html
    ```

## Contributing changes

* Take a quick look at the [HotDoc documentation](https://hotdoc.github.io/)

* Identify the page where you wish to make a change, for example if the name of the page is `vm-setup.html`, the source is `docs/vm-setup.md`.

* Edit the source with the editor of your choice, save your changes.

* Rebuild the documentation:

    ```
    hotdoc run
    ```

* Visit the page again, you should see your changes.

* Commit your changes with a helpful message and submit them upstream.

> Images are stored in the `media/` folder

## Building the documentation for usage on appdev.apertis.org

This part will be handled by upstream for now, so the only requirement for documentation contributors here is to make sure the changes build as intended with the standalone, local version.

For contributors to the application developer portal (backworth), the steps to build the documentation are:

* Make sure to be outside the backworth environment, use `deactivate` for this purpose.

* Make sure pip and virtualenv are installed on your system, on Ubuntu:

    ```
    sudo apt-get install python-pip
    sudo pip install virtualenv
    ```

    and on Fedora:

    ```
    sudo dnf install python-pip
    sudo pip install virtualenv
    ```

* Install hotdoc dependencies, on Ubuntu:

    ```
    sudo apt-get install python-dev libgraphviz-dev libxml2-dev libxslt1-dev cmake
    ```

    and on Fedora:

    ```
    sudo dnf install graphviz-devel python-devel libxml2-devel libxslt-devel cmake
    ```

* Install a recent enough version of hotdoc (>= 0.7.9):

    ```
    virtualenv hotdoc_venv
    . hotdoc_venv/bin/activate
    pip install hotdoc hotdoc-search-extension
    ```

* Navigate to the dist subdirectory:

    ```
    cd dist
    ```

* Create an egg_info for the apertis extension:

    ```
    python setup.py egg_info
    ```

* You can now build the documentation with the provided Makefile:

    ```
    make
    ```

* Copy the built html to the website documentation subfolder:

    ```
    cp -r built_doc/html /path/to/website/root/documentation/static/documentation
    ```

* You can now switch to the website venv and collect static files:

    ```
    deactivate
    . path/to/backworth/env/bin/activate
    cd /path/to/website/
    python manage.py collectstatic --no-input
    ```
