apertis-docs (1.1709.0) 17.09; urgency=medium

  [ Sjoerd Simons ]
  * Build and deploy through jenkins

  [ Emanuele Aina ]
  * Drop empty metadata, avoid an hotdoc warning
  * Fix the description for Barkway
  * Fix link to the HotDoc documentation
  * Change output directory to `./build`
  * Switch to git.mk
  * Rename markdown_files/*.markdown to docs/*.md
  * Add `make pdf` target
  * Add missing appdev pages to the sitemap
  * Fix licensing page rendering by using explicit code blocks
  * bundle-spec: Do not rely on HotDoc-generated anchors
  * Add a quick guide for platform developers
  * Deploy to a more sensible location

  [ Nandini Raju ]
  * appdev-hello-world: Update section about Eclipse
    (Apertis: T4101)

 -- Emanuele Aina <emanuele.aina@collabora.com>  Sat, 26 Aug 2017 15:47:44 +0200

apertis-docs (1.1706.1) 17.06; urgency=medium

  [ Shane Fagan ]
  * Removed references to 16.09

  [ Emanuele Aina ]
  * api: Drop duplicated Tinwell entry
  * api: Link to Traprain and Rhosydd

  [ Justin Kim ]
  * Re-indent Apparmor profile
  * Add D-Bus bind for UI
  * Use the correct name of the AppArmor profile
  * Use <abstractions/dbus-session-strict>
  * Bundle spec: Add <abstractions/fonts> to AppArmor profile
  * Add 'how to use ade' (Apertis: T3907)
  * hello-world: Add a page link to the ADE tool

 -- Emanuele Aina <emanuele.aina@collabora.com>  Fri, 16 Jun 2017 13:22:54 +0200

apertis-docs (1.1706.0) 17.06; urgency=medium

  [ Shane Fagan ]
  * Move to docs.apertis.org
  * Remove Chalgrove and Corbridge from site
  * Add Ribchester to index and api markdown
  * Remove version numbers from documentation

  [ Simon McVittie ]
  * New branch-based versioning scheme for this package, matching what we
    do for code
  * debian: Depend on hotdoc 0.8
  * design: Describe bundle IDs and entry points (Apertis: T3489)
  * gsettings: Describe the required naming convention for schemas
    (Apertis: T3489)
  * Bundle spec:
    - Document <custom> (Apertis: T3490)
    - Call out what the replacements for X-Apertis-Settings* are
      (Apertis: T3489)
    - Mark manifest URL as confirmed to be obsolete (Apertis: T3494)
    - Mark X-Apertis-DataExchangeRules as confirmed obsolete (Apertis: T3493)
    - Say X-Apertis-WindowName is obsolete (Apertis: T3459)
    - Document D-Bus activation (Apertis: T3559)
      + Push implementors towards DBusActivatable via examples
      + Recommend AppArmor rules sufficient for D-Bus activation
    - Store app-bundles SHOULD NOT use menu-entry in Exec

 -- Simon McVittie <smcv@collabora.com>  Wed, 29 Mar 2017 13:29:56 +0100

apertis-docs (0.20161215.0) 16.12; urgency=medium

  [ Simon McVittie ]
  * Bundle spec: use a simpler mechanism to find user-facing schema
  * Bundle spec: forbid <metadata> for now
  * Bundle spec: fix broken links
  * Bundle spec: specifically say we don't include rationale
  * Bundle spec: clarify use of the various flavours of name
  * Bundle spec: summarize fields/tags of at least optional status
  * Bundle spec: add cross-reference links for AppArmor
  * Bundle spec: clarify who provides icons
  * Say that the main entry point must be graphical for now
  * Bundle spec: all D-Bus names must match entry point IDs
  * Add debian/source/apertis-component marking this as a development package
  * Bundle spec: make XDG_DATA_DIRS a link to its spec
  * Bundle spec: add a cross-reference to the Preferences design
  * Bundle spec: add a cross-reference to the i18n design
  * Bundle spec: describe how far this applies to built-in bundles
  * Bundle spec: GSettings schemas must be compiled
  * Declare this to be bundle spec 1.0.0 (Apertis: T2717)

  [ Micah Fedke ]
  * index: Updated index page according to Getting Started proposal
  * apis: Added APIs page according to Getting Started proposal
  * faq: Added FAQ page according to Getting Started proposal

  [ Ekaterina Gerasimova ]
  * Licensing: add basic information
  * Licensing: fix copyright info
  * App distribution: add some basic info

  [ Micah Fedke ]
  * Agents: add section on how to describe them.

  [ Gustavo Noronha Silva ]
  * Widgets: add catalogue

  [ Micah Fedke ]
  * Review and update existing Getting Started pages
  * Remove unused/orphaned files

  [ Ekaterina Gerasimova ]
  * Add Apertis version headers to all documents

  [ Micah Fedke ]
  * Add note for package requests
  * Add short description to the bundle spec doc
  * Add license and author metadata where needed

  [ Ekaterina Gerasimova ]
  * Appdev: design guidelines

  [ Micah Fedke ]
  * Cleanup compile warnings/errors and spelling

  [ Ekaterina Gerasimova ]
  * Programming guide: add programming recommendations
  * Fix links in getting started

  [ Andrew Shadura ]
  * Use correct variable substitution syntax.

 -- Andrew Shadura <andrew.shadura@collabora.co.uk>  Thu, 15 Dec 2016 14:03:46 +0100

apertis-docs (0.20160926.1) 16.09; urgency=low

  * Initial packaging
  * Add draft version of Apertis Application Bundle specification
    (Apertis: T2518)

 -- Simon McVittie <simon.mcvittie@collabora.co.uk>  Mon, 26 Sep 2016 10:55:24 +0100
